package com.devcamp.pizza365.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.pizza365.model.COrder;
import java.util.List;


public interface IOrderRepository extends JpaRepository<COrder, Long> {
	COrder findByOrderId(String orderId);
	List<COrder> findByEmail(String email);
}
