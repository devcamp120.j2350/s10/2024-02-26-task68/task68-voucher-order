package com.devcamp.pizza365.controller;

import com.devcamp.pizza365.model.CMenu;
import com.devcamp.pizza365.model.COrder;
import com.devcamp.pizza365.model.CUser;
import com.devcamp.pizza365.repository.IMenuRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/")
public class CMenuController {
	@Autowired
	IMenuRepository pMenuRepository;
	
	@GetMapping("/menu")
	public ResponseEntity<List<CMenu>> getAllMenu(){
		try {
			List<CMenu> pMenuList = new ArrayList<CMenu>();
			
			pMenuRepository.findAll().forEach(pMenuList::add);
			
			return new ResponseEntity<>(pMenuList, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/menu/{id}")
	public ResponseEntity<CMenu> getMenuById(@PathVariable Long id){
		try {			
			Optional<CMenu> cMenuData = pMenuRepository.findById(id);

			if (cMenuData.isPresent()) {
				return new ResponseEntity<>(cMenuData.get(), HttpStatus.OK);
			}

			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@PostMapping("/menu")
	public ResponseEntity<CMenu> createMenu(@Valid @RequestBody CMenu cMenu){
		try {			
			CMenu cNewMenu =  new CMenu();
			cNewMenu.setDonGia(cMenu.getDonGia());
			cNewMenu.setDuongKinh(cMenu.getDuongKinh());
			cNewMenu.setSalad(cMenu.getSalad());
			cNewMenu.setSize(cMenu.getSize());
			cNewMenu.setSoLuongNuocNgot(cMenu.getSoLuongNuocNgot());
			cNewMenu.setSuon(cMenu.getSuon());
			
			CMenu savedMenu = pMenuRepository.save(cNewMenu);

			return new ResponseEntity<>(savedMenu, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@PutMapping("/menu/{id}")
	public ResponseEntity<CMenu> updateMenu(@PathVariable Long id, @RequestBody CMenu cMenuInput){
		try {			
			Optional<CMenu> cMenuData = pMenuRepository.findById(id);

			if (cMenuData.isPresent()) {
				CMenu cMenu = cMenuData.get();
				cMenu.setDonGia(cMenuInput.getDonGia());
				cMenu.setDuongKinh(cMenuInput.getDuongKinh());
				cMenu.setSalad(cMenuInput.getSalad());
				cMenu.setSize(cMenuInput.getSize());
				cMenu.setSoLuongNuocNgot(cMenuInput.getSoLuongNuocNgot());
				cMenu.setSuon(cMenuInput.getSuon());

				return new ResponseEntity<>(pMenuRepository.save(cMenu), HttpStatus.OK);				
			}

			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping("/menu/{id}")
	public ResponseEntity<CMenu> deleteMenu(@PathVariable Long id){
		try {			
			Optional<CMenu> cMenuData = pMenuRepository.findById(id);

			if (cMenuData.isPresent()) {
				CMenu cMenu = cMenuData.get();
				pMenuRepository.delete(cMenu);

				return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
			}

			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
