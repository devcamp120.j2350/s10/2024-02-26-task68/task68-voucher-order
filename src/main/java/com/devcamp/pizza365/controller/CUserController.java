package com.devcamp.pizza365.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.model.COrder;
import com.devcamp.pizza365.model.CUser;
import com.devcamp.pizza365.repository.IOrderRepository;
import com.devcamp.pizza365.repository.IUserRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class CUserController {
	
	@Autowired
	IUserRepository pUserRepository;

	@Autowired
	IOrderRepository pOrderRepository;
	
	@GetMapping("/users")
	public ResponseEntity<List<CUser>> getUser(){
		try {
			List<CUser> pUsers = new ArrayList<CUser>();
			
			pUserRepository.findAll().forEach(pUsers::add);
			
			return new ResponseEntity<>(pUsers, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@GetMapping("/users/{id}")
	public ResponseEntity<CUser> getUserById(@PathVariable Long id){
		try {			
			Optional<CUser> cUserData = pUserRepository.findById(id);

			if (cUserData.isPresent()) {
				return new ResponseEntity<>(cUserData.get(), HttpStatus.OK);
			}

			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@PostMapping("/users")
	public ResponseEntity<CUser> createUser(@Valid @RequestBody CUser cUser){
		try {			
			CUser cNewUser =  new CUser();
			cNewUser.setFirstname(cUser.getFirstname());
			cNewUser.setEmail(cUser.getEmail());
			cNewUser.setLastname(cUser.getLastname());
			cNewUser.setUsername(cUser.getUsername());
			cNewUser.setSubject(cUser.getSubject());
			
			CUser savedUser = pUserRepository.save(cNewUser);

			return new ResponseEntity<>(savedUser, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@DeleteMapping("/users/{id}")
	public ResponseEntity<CUser> deleteUser(@PathVariable Long id){
		try {			
			Optional<CUser> cUserData = pUserRepository.findById(id);

			if (cUserData.isPresent()) {
				CUser cUser = cUserData.get();
				pUserRepository.delete(cUser);

				// Delete all order which have same user's email.
				List<COrder> orders = pOrderRepository.findByEmail(cUser.getEmail());
				pOrderRepository.deleteAll(orders);
				
				return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
			}

			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@PutMapping("/users/{id}")
	public ResponseEntity<CUser> updateUser(@PathVariable Long id, @RequestBody CUser cUserInput){
		try {			
			Optional<CUser> cUserData = pUserRepository.findById(id);

			if (cUserData.isPresent()) {
				CUser cUser = cUserData.get();

				cUser.setFirstname(cUserInput.getFirstname());
				cUser.setEmail(cUserInput.getEmail());
				cUser.setLastname(cUserInput.getLastname());
				// cUser.setUsername(cUserInput.getUsername());
				cUser.setSubject(cUserInput.getSubject());

				pUserRepository.save(cUser);

				return new ResponseEntity<>(cUser, HttpStatus.OK);				
			}

			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@GetMapping("/user")
	public ResponseEntity<CUser> getUser(@RequestParam(value = "username") String username){
		try {			
			CUser cUser = pUserRepository.findByUsername(username);
			return new ResponseEntity<>(cUser, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@GetMapping("/useremail")
	public ResponseEntity<CUser> getUserByEmail(@RequestParam(value = "email") String email){
		try {			
			CUser cUser = pUserRepository.findByEmail(email);
			return new ResponseEntity<>(cUser, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
