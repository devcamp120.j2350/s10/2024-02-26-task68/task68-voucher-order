package com.devcamp.pizza365.controller;

import com.devcamp.pizza365.model.CDrink;
import com.devcamp.pizza365.model.CMenu;
import com.devcamp.pizza365.repository.IDrinkRepository;

import java.security.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.zip.CRC32;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("/")
public class CDrinkController {
	@Autowired 
	IDrinkRepository pDrinkRepository;
	
	@GetMapping("/drinks")
	public ResponseEntity<List<CDrink>> getDrinkList(){
		try {
			List<CDrink> pDrinkLists = new ArrayList<CDrink>();
			
			pDrinkRepository.findAll().forEach(pDrinkLists::add);
			
			return new ResponseEntity<>(pDrinkLists, HttpStatus.OK);
		} catch (Exception e){
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/drinks/{id}")
	public ResponseEntity<CDrink> getDrinkById(@PathVariable Long id){
		try {			
			Optional<CDrink> cDrinkData = pDrinkRepository.findById(id);

			if (cDrinkData.isPresent()) {
				return new ResponseEntity<>(cDrinkData.get(), HttpStatus.OK);
			}

			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@PostMapping("/drinks")
	public ResponseEntity<CDrink> createMenu(@Valid @RequestBody CDrink cDrink){
		try {			
			CDrink cNewDrink =  new CDrink();
			cNewDrink.setDonGia(cDrink.getDonGia());
			cNewDrink.setMaNuocUong(cDrink.getMaNuocUong());
			cNewDrink.setTenNuocUong(cDrink.getTenNuocUong());		
			cNewDrink.setNgayCapNhat((new Date()).getTime());
			cNewDrink.setNgayTao((new Date()).getTime());
			
			CDrink savedDrink = pDrinkRepository.save(cNewDrink);

			return new ResponseEntity<>(savedDrink, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@PutMapping("/drinks/{id}")
	public ResponseEntity<CDrink> updateDrink(@PathVariable Long id, @RequestBody CDrink cDrinkInput){
		try {			
			Optional<CDrink> cDrinkData = pDrinkRepository.findById(id);

			if (cDrinkData.isPresent()) {
				CDrink cDrink = cDrinkData.get();
				cDrink.setDonGia(cDrinkInput.getDonGia());
				cDrink.setMaNuocUong(cDrinkInput.getMaNuocUong());
				cDrink.setTenNuocUong(cDrinkInput.getTenNuocUong());		
				cDrink.setNgayCapNhat((new Date()).getTime());

				return new ResponseEntity<>(pDrinkRepository.save(cDrink), HttpStatus.OK);				
			}

			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping("/drinks/{id}")
	public ResponseEntity<CDrink> deleteDrink(@PathVariable Long id){
		try {			
			Optional<CDrink> cDrinkData = pDrinkRepository.findById(id);

			if (cDrinkData.isPresent()) {
				CDrink cDrink = cDrinkData.get();
				pDrinkRepository.delete(cDrink);

				return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
			}

			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
