package com.devcamp.pizza365.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.repository.IOrderRepository;
import com.devcamp.pizza365.repository.IUserRepository;
import com.devcamp.pizza365.model.*;

@CrossOrigin
@RestController
@RequestMapping("/")
public class COrderController {
	
	@Autowired
	IOrderRepository pOrderRepository;

	@Autowired
	IUserRepository pUserRepository;
	
	@GetMapping("/orders")
	public ResponseEntity<List<COrder>> getOrder(){
		try {
			List<COrder> pOrders = new ArrayList<COrder>();
			
			pOrderRepository.findAll().forEach(pOrders::add);
			
			return new ResponseEntity<>(pOrders, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/orders/{id}")
	public ResponseEntity<COrder> getOrderById(@PathVariable Long id){
		try {			
			Optional<COrder> cOrderData = pOrderRepository.findById(id);

			if (cOrderData.isPresent()) {
				return new ResponseEntity<>(cOrderData.get(), HttpStatus.OK);
			}

			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@PostMapping("/orders")
	public ResponseEntity<COrder> createUser(@Valid @RequestBody COrder cOrder){
		try {			
			COrder cNewOrder =  new COrder();
			cNewOrder.setEmail(cOrder.getEmail());
			cNewOrder.setGiamGia(cOrder.getGiamGia());
			cNewOrder.setLoaiPizza(cOrder.getLoaiPizza());
			cNewOrder.setSalad(cOrder.getSalad());
			cNewOrder.setKichCo(cOrder.getKichCo());
			
			COrder savedOrder = pOrderRepository.save(cNewOrder);

			return new ResponseEntity<>(savedOrder, HttpStatus.CREATED);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@DeleteMapping("/orders/{id}")
	public ResponseEntity<COrder> deleteOrder(@PathVariable Long id){
		try {			
			Optional<COrder> cOrderData = pOrderRepository.findById(id);

			if (cOrderData.isPresent()) {
				COrder cOrder = cOrderData.get();
				pOrderRepository.delete(cOrder);				
				
				return new ResponseEntity<>(null, HttpStatus.NO_CONTENT);
			}

			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	@PutMapping("/orders/{id}")
	public ResponseEntity<COrder> updateUser(@PathVariable Long id, @RequestBody COrder cOrderInput){
		try {			
			Optional<COrder> cOrderData = pOrderRepository.findById(id);

			if (cOrderData.isPresent()) {
				COrder cOrder = cOrderData.get();
				cOrder.setEmail(cOrderInput.getEmail());
				cOrder.setGiamGia(cOrderInput.getGiamGia());
				cOrder.setLoaiPizza(cOrderInput.getLoaiPizza());
				cOrder.setSalad(cOrderInput.getSalad());
				cOrder.setKichCo(cOrderInput.getKichCo());

				pOrderRepository.save(cOrder);

				return new ResponseEntity<>(cOrder, HttpStatus.OK);				
			}

			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/users/{id}/orders")
	public ResponseEntity<List<COrder>> getOrdersByUserId(@PathVariable Long id){
		try {			
			Optional<CUser> cUser = pUserRepository.findById(id);

			if (cUser.isPresent()) {
				List<COrder> cOrders = pOrderRepository.findByEmail(cUser.get().getEmail());

				return new ResponseEntity<>(cOrders, HttpStatus.OK);				
			}

			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
